<?php
/**
 * DokuWiki Parabola Template
 * Based on the starter template and a wordpress theme of the same name
 *
 * @link     http://dokuwiki.org/template:parabola
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/runmobilemenu.js"></script>
</head>

<body id="dokuwiki__top" wpclass="post-template-default single single-post postid-9 single-format-standard logged-in admin-bar parabola-image-four caption-light meta-light parabola_triagles parabola-menu-left customize-support" class=" <?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">



<?php //cryout_body_hook(); ?>

<div id="wrapper" class="hfeed">

<?php ////cryout_wrapper_hook(); ?>
<!-- parabola_slefts_socials // parabola_srights_socials -->


<div id="header-full">

<?php tpl_includeFile('header.html') ?>
<header id="header">

<?php //cryout_masthead_hook(); ?>
<!-- parabola_top_menu -->

        <div id="masthead">

            <div id="branding" role="banner">

                <ul class="a11y skip">
                    <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
                </ul>

                <?php //cryout_branding_hook();?>
                <!-- parabola_title_and_description #header-container -->
                <div id="header-container"><div>
                    <div id="site-title"><span><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></span></div>
                    <?php /* how to insert logo instead (if no CSS image replacement technique is used):
                        upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
                        tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
                    <?php if ($conf['tagline']): ?>
                    <div id="site-description"><?php echo $conf['tagline'] ?></div>
                    <?php endif ?>
                </div>

                    <!-- <div class="socials" id="sheader">
                        <a target="_blank" rel="nofollow" href="#" class="socialicons social-YouTube" title="YouTube"><img alt="YouTube" src="http://localhost/wordpress/wp-content/themes/parabola/images/socials/YouTube.png"><div class="socials-hover"></div></a>
                        <a target="_blank" rel="nofollow" href="#" class="socialicons social-Twitter" title="Twitter"><img alt="Twitter" src="http://localhost/wordpress/wp-content/themes/parabola/images/socials/Twitter.png"><div class="socials-hover"></div></a>
                        <a target="_blank" rel="nofollow" href="#" class="socialicons social-RSS" title="RSS"><img alt="RSS" src="http://localhost/wordpress/wp-content/themes/parabola/images/socials/RSS.png"><div class="socials-hover"></div></a>
                    </div> -->
                </div>

                <?php //cryout_header_widgets_hook(); ?>
                <!-- parabola_header_widget -->
                <!-- <div id="header-widget-area">
                    <ul class="yoyo">
                    <li id="meta-4" class="widget-container widget_meta"><h3 class="widget-title">Meta</h3>         
                        <ul>
                        <li><a href="http://localhost/wordpress/wp-admin/">Site Admin</a></li> 
                        <li><a href="http://localhost/wordpress/wp-login.php?action=logout&amp;_wpnonce=31916fb65c">Log out</a></li>
                        <li><a href="#">Entries feed</a></li>
                        <li><a href="#">Comments feed</a></li>
                        <li><a href="#">WordPress.org</a></li>         
                        </ul>
                    </li>
                    </ul>
                </div> -->
                <div style="clear:both;"></div>

            </div><!-- #branding -->
            <a id="nav-toggle"><span>&nbsp;</span></a>
            <nav id="access" role="navigation">

                <?php //cryout_access_hook();?>
                <!-- parabola_main_menu -->
                <div class="skip-link screen-reader-text"><a href="#content" title="Skip to content">Skip to content</a></div>
                <div class="menu"><ul id="prime_nav" class="menu">
                     <!-- SITE TOOLS -->
                        <h3 class="a11y"><?php echo $lang['site_tools'] ?></h3>
                            <?php tpl_toolsevent('sitetools', array(
                                'recent'    => tpl_action('recent', 1, 'li', 1),
                                'media'     => tpl_action('media', 1, 'li', 1),
                                'index'     => tpl_action('index', 1, 'li', 1),
                            )); ?>
                </ul></div>

            </nav><!-- #access -->

        </div><!-- #masthead -->

    <div style="clear:both;height:1px;width:1px;"> </div>

</header><!-- #header -->
</div><!-- #header-full -->
<div id="main">
    <div  id="forbottom" >
        <?php //cryout_forbottom_hook(); ?>
        <!-- parabola_top_menu -->

        <div style="clear:both;"> </div>

        <?php //cryout_breadcrumbs_hook();?>
        <!-- parabola_breadcrumbs -->

<div style="clear:both;"></div>
    


<section id="container" class="two-columns-right" wpclass="<?php //echo parabola_get_layout_class(); ?>">
<div id="content" role="main">
    <?php //cryout_before_content_hook(); ?>

   <!--  <div  <?php //post_class(); ?>>
    <h1 class="entry-title"><?php //the_title(); ?></h1>
    <?php //cryout_post_title_hook(); ?>
    <div class="entry-meta">
    <?php //parabola_meta_before(); cryout_post_meta_hook(); ?>
    </div> --><!-- .entry-meta -->

    <?php html_msgarea() /* occasional error and info messages on top of the page */ ?>

    <!-- BREADCRUMBS -->
    <?php if($conf['breadcrumbs']){ ?>
        <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
    <?php } ?>
    <?php if($conf['youarehere']){ ?>
        <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
    <?php } ?>

    <div class="entry-content">
        <!-- ********** CONTENT ********** -->
        <div id="dokuwiki__content">
            <?php tpl_flush() /* flush the output buffer */ ?>
            <?php tpl_includeFile('pageheader.html') ?>

            <div class="page">
                <!-- wikipage start -->
                <?php tpl_content() /* the main content */ ?>
                <!-- wikipage stop -->
                <div class="clearer"></div>
            </div>

            <?php tpl_flush() ?>
            <?php tpl_includeFile('pagefooter.html') ?>
        </div><!-- /content -->
    <?php //the_content(); ?>
    <?php //wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'parabola' ), 'after' => '</span></div>' ) ); ?>
    </div><!-- .entry-content -->

    <!-- <div id="entry-author-info">
        <div id="author-avatar">
        <?php //echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'parabola_author_bio_avatar_size', 60 ) ); ?>
        </div><!-- #author-avatar -->
        <!--
        <div id="author-description">
            <h4>About this author</h4>
            <?php //the_author_meta( 'description' ); ?>
            <div id="author-link">
                <a href="<?php //echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                <?php //printf( 'View all posts by this author'.'<span class="meta-nav">&rarr;</span>' ); ?>
                </a>
            </div><!-- #author-link -->
        <!--
        </div><!-- #author-description -->
    <!-- </div --><!-- #entry-author-info -->

    <!-- <div class="entry-utility">
    <?php //parabola_posted_in(); ?>
    <?php //edit_post_link( __( 'Edit', 'parabola' ), '<span class="edit-link">', '</span>' ); cryout_post_footer_hook(); ?>
    </div><!-- .entry-utility -->
    <!-- </div><!-- #post-## -->

    <!-- <div id="nav-below" class="navigation">
    <div class="nav-previous"><?php //previous_post_link( '%link', '<span class="meta-nav">&laquo;</span> %title' ); ?></div>
    <div class="nav-next"><?php //next_post_link( '%link', '%title <span class="meta-nav">&raquo;</span>' ); ?></div>
    </div> --><!-- #nav-below -->

    <?php //comments_template( '', true ); ?>


    <?php //cryout_after_content_hook(); ?>
</div><!-- #content -->

<?php //parabola_get_sidebar(); ?>

<div id="secondary" class="widget-area sidey" role="complementary">

    <div id="search-10" class="widget-container widget_search">
    <!-- <form role="search" method="get" class="searchform" action="http://localhost/wordpress/">
    <label>
    <span class="screen-reader-text">Search for:</span>
    <input type="search" class="s" placeholder="SEARCH" value="" name="s">
    </label>
    <button type="submit" class="searchsubmit"><span class="screen-reader-text">Search</span></button>
    </form> -->
    <h3 class="widget-title">Search</h3>  
    <?php tpl_searchform() ?>
    </div>

     <div id="anotherone" class="widget-container">     
        <!-- <h3 class="widget-title">xxx</h3>      -->
       <!-- ********** ASIDE ********** -->
        <?php if ($showSidebar): ?>
            <div id="writtensidebar">
            <?php tpl_includeFile('sidebarheader.html') ?>    
            <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
            <?php tpl_includeFile('sidebarfooter.html') ?>
            <div class="clearer"></div>
            </div><!-- /aside -->
        <?php endif; ?>
    </div>

    <ul class="xoxo">
  
    <?php if ($showTools): ?>
    <li class="widget-container"><h3 class="widget-title">Page Tools</h3>     
         <!-- PAGE ACTIONS -->    
        <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
        <ul>
            <?php tpl_toolsevent('pagetools', array(
                'edit'      => tpl_action('edit', 1, 'li', 1),
                'discussion'=> _tpl_action('discussion', 1, 'li', 1),
                'revisions' => tpl_action('revisions', 1, 'li', 1),
                'backlink'  => tpl_action('backlink', 1, 'li', 1),
                'subscribe' => tpl_action('subscribe', 1, 'li', 1),
                'revert'    => tpl_action('revert', 1, 'li', 1),
                'top'       => tpl_action('top', 1, 'li', 1),
            )); ?>
        </ul>
    </li>   
    <?php endif; ?>    
    <?php if ($conf['useacl'] && $showTools): ?>
    <li class="widget-container">     <h3 class="widget-title">User Tools</h3>      
        <ul>
        <!-- USER TOOLS -->
        <h3 class="a11y"><?php echo $lang['user_tools'] ?></h3>
            <?php
                if (!empty($_SERVER['REMOTE_USER'])) {
                    echo '<li class="user">';
                    tpl_userinfo(); /* 'Logged in as ...' */
                    echo '</li>';
                }
            ?>
            <?php /* the optional second parameter of tpl_action() switches between a link and a button,
                     e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
            ?>
            <?php tpl_toolsevent('usertools', array(
                'admin'     => tpl_action('admin', 1, 'li', 1),
                'userpage'  => _tpl_action('userpage', 1, 'li', 1),
                'profile'   => tpl_action('profile', 1, 'li', 1),
                'register'  => tpl_action('register', 1, 'li', 1),
                'login'     => tpl_action('login', 1, 'li', 1),
            )); ?>
        </ul>
    </li>
    <?php endif ?>
</ul>
   
</div>

</section><!-- #container -->

    <!-- <footer id="footer" role="contentinfo">
        <div id="colophon">
        
            <?php //get_sidebar( 'footer' );?>
            
        </div><!-- #colophon -->
        <!--
        <div id="footer2">
            <div id="footer2-inner">
                <?php //cryout_footer_hook(); ?>
                <!-- parabola_footer_socials // parabola_site_info // parabola_copyright -->
            <!--
            </div>
        </div><!-- #footer2 -->

    <!-- </footer> --><!-- #footer -->

    </div> <!-- #forbottom -->
    </div><!-- #main -->

    <footer id="footer" role="contentinfo">
    <div id="colophon"></div><!-- #colophon -->

    <div id="footer2">
        <div id="footer2-inner">
            <nav class="footermenu"><ul id="menu-social-media-links" class="menu">
            <li class="menu-item"><a href="#">Link 1</a></li>
            <li class="menu-item"><a href="#">Link 2</a></li>
            <li class="menu-item"><a href="#">Link 3</a></li>
            <li class="menu-item"><a href="#">Link 4</a></li>
            </ul></nav>

        <div id="site-copyright">
            <div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
            <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>
            <!-- This text can be changed from the Miscellaneous section of the settings page. <br>
            <b>Lorem ipsum</b> dolor sit amet, <a href="#">consectetur adipiscing</a> elit, cras ut imperdiet augue.  -->
        </div> 
        <div style="text-align:center;padding:5px 0 2px;text-transform:uppercase;font-size:12px;margin:1em auto 0;">
            Powered by <a target="_blank" href="#" title="Parabola Theme by Cryout Creations">Parabola</a> 
            &amp; 
            <a target="_blank" href="#" title="Converted to Dokuwiki by desbest">desbest</a>
            &amp;
            <a target="_blank" href="#" title="Flat file wiki script">  Dokuwiki</a>
            .
        </div><!-- #site-info -->

        <!-- <div class="socials" id="sfooter">
            <a target="_blank" rel="nofollow" href="#" class="socialicons social-YouTube" title="YouTube"><img alt="YouTube" src="http://localhost/wordpress/wp-content/themes/parabola/images/socials/YouTube.png"><div class="socials-hover"></div></a>
            <a target="_blank" rel="nofollow" href="#" class="socialicons social-Twitter" title="Twitter"><img alt="Twitter" src="http://localhost/wordpress/wp-content/themes/parabola/images/socials/Twitter.png"><div class="socials-hover"></div></a>
            <a target="_blank" rel="nofollow" href="#" class="socialicons social-RSS" title="RSS"><img alt="RSS" src="http://localhost/wordpress/wp-content/themes/parabola/images/socials/RSS.png"><div class="socials-hover"></div></a>
        </div>     -->    
        <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
    </div><!-- #footer2 -->

    </footer>
    <?php tpl_includeFile('footer.html') ?>

</div><!-- #wrapper -->


<!-- end of parabola -->


    <?php /* with these Conditional Comments you can better address IE issues in CSS files,
             precede CSS rules by #IE8 for IE8 (div closes at the bottom) */ ?>
    <!--[if lte IE 8 ]><div id="IE8"><![endif]-->

    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>


</body>
</html>
