# Parabola dokuwiki template

* Designed by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:parabola)

![parabola theme screenshot](https://i.imgur.com/Sl21GOq.png)